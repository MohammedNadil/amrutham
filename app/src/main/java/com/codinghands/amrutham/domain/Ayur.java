package com.codinghands.amrutham.domain;

/**
 * Created by Nadil on 5/30/2017.
 */

public class Ayur {


    String descri, image,name;

    public Ayur() {
        super();
    }

    public Ayur(String descri, String image, String name) {
        this.descri = descri;
        this.image = image;
        this.name = name;
    }

    public String getDescri() {
        return descri;
    }

    public void setDescri(String descri) {
        this.descri = descri;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Ayur{" +
                "descri='" + descri + '\'' +
                ", image='" + image + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
