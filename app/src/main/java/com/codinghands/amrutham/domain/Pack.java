package com.codinghands.amrutham.domain;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Nadil on 5/31/2017.
 */

public class Pack {


    String days,image;
    ArrayList<Map<String, Object>> description;
    String names, nights, type;

    public Pack() {
        super();
    }

    public Pack(String days, String image, ArrayList<Map<String, Object>> description, String names, String nights, String type) {
        this.days = days;
        this.image = image;
        this.description = description;
        this.names = names;
        this.nights = nights;
        this.type = type;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<Map<String, Object>> getDescription() {
        return description;
    }

    public void setDescription(ArrayList<Map<String, Object>> description) {
        this.description = description;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getNights() {
        return nights;
    }

    public void setNights(String nights) {
        this.nights = nights;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Pack{" +
                "days='" + days + '\'' +
                ", image='" + image + '\'' +
                ", description=" + description +
                ", names='" + names + '\'' +
                ", nights='" + nights + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
