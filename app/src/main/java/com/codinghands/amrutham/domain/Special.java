package com.codinghands.amrutham.domain;

/**
 * Created by Nadil on 6/23/2017.
 */

public class Special {
    String url;
    String name;

    public Special() {
        super();
    }

    public Special(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Special{" +
                "url='" + url + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
