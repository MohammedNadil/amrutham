package com.codinghands.amrutham.domain;

/**
 * Created by Nadil on 6/1/2017.
 */

public class Ved {
    String url,img;

    public Ved() {super();
    }

    public Ved(String url, String img) {
        this.url = url;
        this.img = img;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "Ved{" +
                "url='" + url + '\'' +
                ", img='" + img + '\'' +
                '}';
    }
}
