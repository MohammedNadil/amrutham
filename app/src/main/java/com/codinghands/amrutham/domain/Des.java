package com.codinghands.amrutham.domain;

/**
 * Created by Nadil on 5/31/2017.
 */

public class Des {


    String title,content;

    public Des() {
        super();
    }

    public Des(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Des{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
