package com.codinghands.amrutham.domain;

/**
 * Created by Nadil on 6/1/2017.
 */

public class Slidehome {
    String url;

    public Slidehome() {super();
    }

    public Slidehome(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Gal{" +
                "url='" + url + '\'' +
                '}';
    }
}
