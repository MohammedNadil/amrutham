package com.codinghands.amrutham.adapter;

/**
 * Created by Nadil on 6/2/2017.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.activity.TouchImageView;
import com.codinghands.amrutham.domain.Gal;
import com.squareup.picasso.Picasso;


import java.util.List;

public class MyAdapter3 extends PagerAdapter {

    private List<Gal> images;
    private LayoutInflater inflater;
    private Context context;

    Gal gal;

    public MyAdapter3(Context context, List<Gal> images) {
        this.context = context;
        this.images = images;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.slide3, view, false);
        TouchImageView myImage = (TouchImageView) myImageLayout
                .findViewById(R.id.image);


        gal = images.get(position);

        Picasso.with(context).load(gal.getUrl()).into(myImage);

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}