package com.codinghands.amrutham.adapter;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.activity.Desview;
import com.codinghands.amrutham.activity.Mesg;
import com.codinghands.amrutham.domain.Pack;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class PackAdapter extends RecyclerView.Adapter<PackAdapter.MyViewHolder> {
    private Context mContext;
    public static List<Pack> packs;
    public static Pack pack;




    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView packimg,love;
        public ImageView enq;
        public TextView days;
        public TextView nights;
        public TextView title;
        public CardView cardView;


        public MyViewHolder(final View view) {
            super(view);


            packimg = (ImageView) view.findViewById(R.id.packimage);
            days = (TextView) view.findViewById(R.id.days);
            title = (TextView) view.findViewById(R.id.pheading);
            nights = (TextView) view.findViewById(R.id.nights);
            cardView = (CardView) view.findViewById(R.id.pcard);
            love= (ImageView) view.findViewById(R.id.love);

            enq = (ImageView) view.findViewById(R.id.enquiry);

        }
    }

    public PackAdapter(Context mContext, List<Pack> packs) {
        this.mContext = mContext;
        this.packs = packs;


    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.packcard, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        pack = packs.get(position);

        if (pack.getType().equals("Honeymoon")) {
            holder.love.setVisibility(View.VISIBLE);
        }



        final Intent intent = new Intent(mContext, Desview.class);
        intent.putExtra("pos", position);
        intent.putExtra("img",pack.getImage());


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mContext.startActivity(intent);
            }
        });

        final FragmentManager manager = ((FragmentActivity) mContext).getSupportFragmentManager();
        final Bundle bundle = new Bundle();
        bundle.putInt("pos", position);



        holder.enq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent1=new Intent(mContext, Mesg.class);
                intent1.putExtra("pos", position);
                intent1.putExtra("boolean",true);
                mContext.startActivity(intent1);

            }
        });


        holder.title.setText(pack.getNames());


        Log.d("pac","namename"+pack.getNames());
        holder.days.setText(pack.getDays()+" "+"Days");
        holder.nights.setText(pack.getNights()+" "+"Nights");
        Glide.with(mContext).load(pack.getImage()).placeholder(R.drawable.placeholder).into(holder.packimg);

    }


    @Override
    public int getItemCount() {
        Log.d("response", " value students : " + packs);
        return packs.size();
    }
}
