package com.codinghands.amrutham.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.widget.AutoScrollHelper;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.domain.Ayur;


import java.util.List;


public class CardAdapter extends RecyclerView.Adapter<CardAdapter.MyViewHolder> {
    private Context mContext;
    public static List<Ayur> ayurs;
    public static Ayur ayur;
    int pos = -1;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView ayuimg;
        public TextView head;
        public CardView cardView;
        public View mar, mar2;
        TextView des;


        public MyViewHolder(final View view) {
            super(view);


            ayuimg = (ImageView) view.findViewById(R.id.ayuimage);
            head = (TextView) view.findViewById(R.id.heading);
            cardView = (CardView) view.findViewById(R.id.card);
            des = (TextView) view.findViewById(R.id.desciption);
            mar = view.findViewById(R.id.margin);
            mar2 = view.findViewById(R.id.margin2);

        }

    }


    public CardAdapter(Context mContext, List<Ayur> ayurs) {
        this.mContext = mContext;
        this.ayurs = ayurs;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ayurcard, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ayur = ayurs.get(position);
        String url = ayur.getImage();

        Glide.with(mContext).load(url).placeholder(R.drawable.placeholder).into(holder.ayuimg);


        holder.head.setText(ayur.getName());
        holder.des.setText(ayur.getDescri());

        if (position == 0) {
            holder.mar.setBackgroundColor(Color.parseColor("#2f7d00"));
            holder.mar2.setBackgroundColor(Color.parseColor("#2f7d00"));
            holder.head.setTextColor(Color.parseColor("#2f7d00"));
        } else if (position < 6) {
            holder.mar.setBackgroundColor(Color.parseColor("#2f7d" + (position * 15)));
            holder.mar2.setBackgroundColor(Color.parseColor("#2f7d00"));
            holder.head.setTextColor(Color.parseColor("#2f7d" + (position * 15)));
        } else {
            holder.mar.setBackgroundColor(Color.parseColor("#2f7e00"));
            holder.mar2.setBackgroundColor(Color.parseColor("#2f7d00"));
            holder.head.setTextColor(Color.parseColor("#2f7e00"));
        }

        if (pos == position) {

            holder.des.setVisibility(View.VISIBLE);
        } else {

            holder.des.setVisibility(View.GONE);

        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (position != pos) {
                    holder.des.setVisibility(View.VISIBLE);

                    notifyDataSetChanged();
                    pos = position;

                } else {


                    holder.des.setVisibility(View.GONE);

                    notifyDataSetChanged();
                    pos = -1;
                }

            }
        });

    }

    @Override
    public int getItemCount() {

        Log.d("response", " value students : " + ayurs);
        return ayurs.size();
    }
}
