package com.codinghands.amrutham.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.activity.Fullview;
import com.codinghands.amrutham.domain.Gal;


import java.util.List;


public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {
    private Context mContext;
    public static List<Gal> gals;
    public static Gal gal;
    CardView cardView;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView galimg;
        public MyViewHolder(final View view) {
            super(view);
            galimg = (ImageView) view.findViewById(R.id.galimg);
            cardView = (CardView) view.findViewById(R.id.gal);


        }
    }

    public GalleryAdapter(Context mContext, List<Gal> gals) {
        this.mContext = mContext;
        this.gals = gals;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.galcard, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        gal = gals.get(position);
        Glide.with(mContext).load(gal.getUrl()).placeholder(R.drawable.placeholder).into(holder.galimg);

        final Intent intent = new Intent(mContext, Fullview.class);
        intent.putExtra("pos", position);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mContext.startActivity(intent);

            }
        });


    }


    @Override
    public int getItemCount() {
        Log.d("response", " value students : " + gals);
        return gals.size();
    }
}
