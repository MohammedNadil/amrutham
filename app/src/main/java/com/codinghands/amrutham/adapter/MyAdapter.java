package com.codinghands.amrutham.adapter;

/**
 * Created by Nadil on 6/2/2017.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.domain.Slidehome;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends PagerAdapter {

    private List<Slidehome> images;
    private LayoutInflater inflater;
    private Context context;
    public static Slidehome slidehome;


    public MyAdapter(Context context, List<Slidehome> images) {
        this.context = context;
        this.images = images;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.slide, view, false);

        ImageView myImage = (ImageView) myImageLayout
                .findViewById(R.id.sliderhome);
        slidehome = images.get(position);

        Picasso.with(context).load(slidehome.getUrl()).placeholder(R.drawable.placeholder).into(myImage);

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }


}
