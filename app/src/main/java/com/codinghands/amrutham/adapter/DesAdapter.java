package com.codinghands.amrutham.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codinghands.amrutham.R;
import com.codinghands.amrutham.domain.Des;


import java.util.List;


public class DesAdapter extends RecyclerView.Adapter<DesAdapter.MyViewHolder> {
    private Context mContext;
    List<Des> des;
    public static Des desc;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView head;
        public TextView des;


        public MyViewHolder(final View view) {
            super(view);


            head = (TextView) view.findViewById(R.id.head);
            des = (TextView) view.findViewById(R.id.des);

        }
    }

    public DesAdapter(Context mContext, List<Des> des) {
        this.mContext = mContext;
        this.des = des;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.descard, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        desc = des.get(position);

        holder.head.setText(desc.getTitle());
        holder.des.setText(desc.getContent());

    }


    @Override
    public int getItemCount() {

        Log.d("response", " value students : " + des);
        return des.size();
    }
}
