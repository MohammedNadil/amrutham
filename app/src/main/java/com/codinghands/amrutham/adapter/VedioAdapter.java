package com.codinghands.amrutham.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.codinghands.amrutham.R;

import com.codinghands.amrutham.activity.VedioViews;
import com.codinghands.amrutham.domain.Ved;
import com.codinghands.amrutham.fragments.Vedio;


import java.io.IOException;
import java.util.List;


public class VedioAdapter extends RecyclerView.Adapter<VedioAdapter.MyViewHolder> {
    private Context mContext;
    public static List<Ved> veds;
    CardView cardView;

    Ved ved;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView vimg;

        public MyViewHolder(final View view) {
            super(view);

            vimg = (ImageView) view.findViewById(R.id.vimg);
            cardView = (CardView) view.findViewById(R.id.ved);


        }
    }

    public VedioAdapter(Context mContext, List<Ved> veds) {
        this.mContext = mContext;
        this.veds = veds;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vcard, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        ved = veds.get(position);

        Glide.with(mContext).load(ved.getImg()).placeholder(R.drawable.placeholder).into(holder.vimg);

        final Intent intent = new Intent(mContext, VedioViews.class);
        intent.putExtra("pos", position);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mContext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        Log.d("response", " value students : " + veds);
        return veds.size();
    }
}
