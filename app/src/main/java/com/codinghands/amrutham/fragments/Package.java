package com.codinghands.amrutham.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.bumptech.glide.Glide;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.adapter.PackAdapter;
import com.codinghands.amrutham.domain.Pack;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by Nadil on 5/31/2017.
 */

public class Package extends android.support.v4.app.Fragment {

    private PackAdapter adapter;
    LinearLayout reload;
    View layout;
    RecyclerView recyclerView;
    private static final String TAG = "Ayur";
    List<Pack> packList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.packlist, container, false);
        reload = (LinearLayout) layout.findViewById(R.id.reload);
        packList = new ArrayList<>();
        recyclerView = (RecyclerView) layout.findViewById(R.id.pack_list);
        recyclerView.setNestedScrollingEnabled(false);
        getActivity().setTitle("All Packages");
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        ImageView load= (ImageView) layout.findViewById(R.id.loading);
        Glide.with(this).load(R.drawable.loading).asGif().into(load);



        DatabaseReference myRef2 = mDatabase.getReference().child("packages");
        myRef2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                packList.clear();
                reload.setVisibility(View.GONE);
                try {

                    ArrayList<Map<String, Object>> pack2 = (ArrayList<Map<String, Object>>) dataSnapshot.child("packages").getValue();
                    for (int i = 0; i < pack2.size(); i++) {
                        String days = (String) pack2.get(i).get("days");

                        ArrayList<Map<String, Object>> dess = (ArrayList<Map<String, Object>>) pack2.get(i).get("description");
                        String image = (String) pack2.get(i).get("image");
                        String names = (String) pack2.get(i).get("name");
                        String nights = (String) pack2.get(i).get("nights");
                        String type = (String) pack2.get(i).get("type");


                        Pack pack = new Pack(days, image, dess, names, nights, type);
                        packList.add(pack);

                        Log.d(TAG, "paclist" + packList);
                    }
                    adapter = new PackAdapter(getActivity(), packList);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setAdapter(adapter);


                } catch (Exception e) {
                    Log.d(TAG, "error" + e);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
                reload.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        });


        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Package aPackage = new Package();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.slider, aPackage).commit();

            }
        });

        return layout;
    }

}
