package com.codinghands.amrutham.fragments;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.bumptech.glide.Glide;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.activity.Tab;
import com.codinghands.amrutham.adapter.VedioAdapter;
import com.codinghands.amrutham.domain.Ved;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by Nadil on 5/30/2017.
 */

public class Vedio extends Fragment {
    private VedioAdapter adapter;
    private RecyclerView recyclerView;
    private static final String TAG = "Ayur";
    LinearLayout reload;
    List<Ved> vedList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.vediolist, container, false);
        vedList = new ArrayList<>();

        reload = (LinearLayout) layout.findViewById(R.id.reload);
        recyclerView = (RecyclerView) layout.findViewById(R.id.vediolist);

        ImageView load= (ImageView) layout.findViewById(R.id.loading);
        Glide.with(this).load(R.drawable.loading).asGif().into(load);


        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();

        DatabaseReference myRef4 = mDatabase.getReference().child("video");
        myRef4.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                vedList.clear();
                reload.setVisibility(View.GONE);
                try {

                    ArrayList<Map<String, Object>> ved = (ArrayList<Map<String, Object>>) dataSnapshot.child("videos").getValue();
                    for (int i = 0; i < ved.size(); i++) {

                        String vimage = String.valueOf(ved.get(i).get("vedioimage"));
                        String veds = String.valueOf(ved.get(i).get("vediourl"));

                        Ved ved1 = new Ved(veds, vimage);

                        vedList.add(ved1);
                        Log.d(TAG, "vedio" + vedList);
                    }
                } catch (Exception e) {
                    Log.d(TAG, "error" + e);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());

                reload.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        });


        recyclerView.setNestedScrollingEnabled(false);
        adapter = new VedioAdapter(getActivity(), vedList);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 6));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        }

        recyclerView.setAdapter(adapter);

        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Tab tab = new Tab();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.slider, tab).commit();

            }
        });
        return layout;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ? 6 : 4));
        super.onConfigurationChanged(newConfig);
    }
}