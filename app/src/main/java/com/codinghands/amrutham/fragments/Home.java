package com.codinghands.amrutham.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.codinghands.amrutham.R;
import com.codinghands.amrutham.activity.PackDisplay;
import com.codinghands.amrutham.adapter.MyAdapter;
import com.codinghands.amrutham.domain.Slidehome;
import com.codinghands.amrutham.domain.Special;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import me.relex.circleindicator.CircleIndicator;


/**
 * Created by Nadil on 5/30/2017.
 */

public class Home extends android.support.v4.app.Fragment {

    private static AutoScrollViewPager mPager;
    RelativeLayout im1, im2, im3, im4;
    View vid;
    public List<Slidehome> slider;

    ImageView img1, img2, img3, img4;
    TextView t1, t2, t3, t4;
    LinearLayout reload;
    List<Special> specials;
    MyAdapter myAdapter;
    Special special2;
    View layout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.home, container, false);
        reload = (LinearLayout) layout.findViewById(R.id.reload);

        vid = (View) layout.findViewById(R.id.vid);

        getActivity().setTitle("Amrutham Holidays");
        slider = new ArrayList<>();
        specials = new ArrayList<>();

        ImageView load= (ImageView) layout.findViewById(R.id.loading);
        Glide.with(this).load(R.drawable.loading).asGif().into(load);



        mPager = (AutoScrollViewPager) layout.findViewById(R.id.pager);
        img1 = (ImageView) layout.findViewById(R.id.flag1);
        t1 = (TextView) layout.findViewById(R.id.flagt1);
        img2 = (ImageView) layout.findViewById(R.id.flag2);
        t2 = (TextView) layout.findViewById(R.id.flagt2);
        img3 = (ImageView) layout.findViewById(R.id.flag3);
        t3 = (TextView) layout.findViewById(R.id.flagt3);
        img4 = (ImageView) layout.findViewById(R.id.flag4);
        t4 = (TextView) layout.findViewById(R.id.flagt4);


        im1 = (RelativeLayout) layout.findViewById(R.id.im1);
        im2 = (RelativeLayout) layout.findViewById(R.id.im2);
        im3 = (RelativeLayout) layout.findViewById(R.id.im3);
        im4 = (RelativeLayout) layout.findViewById(R.id.im4);


        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();


        DatabaseReference myRef5 = mDatabase.getReference().child("Homeslider");

        myRef5.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                slider.clear();
                reload.setVisibility(View.GONE);
                try {

                    ArrayList<Map<String, Object>> homeslider = (ArrayList<Map<String, Object>>) dataSnapshot.child("images").getValue();

                    Log.d("tag", "imagessss:" + homeslider);

                    for (int i = 0; i < homeslider.size(); i++) {


                        String imgs = String.valueOf(homeslider.get(i));
                        Slidehome slidehome = new Slidehome(imgs);

                        slider.add(slidehome);

                        myAdapter = new MyAdapter(getActivity(), slider);

                        mPager.setAdapter(myAdapter);
                        mPager.setOnPageChangeListener(new MyOnPageChangeListener());
                        myAdapter.notifyDataSetChanged();
                        mPager.setInterval(2000);
                        mPager.startAutoScroll();
                        CircleIndicator indicator = (CircleIndicator) layout.findViewById(R.id.indicator);
                        indicator.setViewPager(mPager);
                        Log.d("tag", "imagessss:" + slidehome);
                    }


                    ArrayList<Map<String, Object>> special = (ArrayList<Map<String, Object>>) dataSnapshot.child("special").getValue();


                    for (int i = 0; i < special.size(); i++) {


                        String imges = String.valueOf(special.get(i).get("img"));
                        String nme = String.valueOf(special.get(i).get("name"));
                        Special special1 = new Special(imges, nme);

                        specials.add(special1);


                    }


                    for (int i = 0; i < specials.size(); i++) {

                        special2 = specials.get(i);
                        if (i == 0) {
                            Glide.with(getActivity()).load(special2.getUrl()).placeholder(R.drawable.placeholder).into(img1);
                            t1.setText(special2.getName());
                        } else if (i == 1) {
                            Glide.with(getActivity()).load(special2.getUrl()).placeholder(R.drawable.placeholder).into(img2);
                            t2.setText(special2.getName());
                        } else if (i == 2) {
                            Glide.with(getActivity()).load(special2.getUrl()).placeholder(R.drawable.placeholder).into(img3);
                            t3.setText(special2.getName());
                        } else if (i == 3) {
                            Glide.with(getActivity()).load(special2.getUrl()).placeholder(R.drawable.placeholder).into(img4);
                            t4.setText(special2.getName());
                        }
                    }


                } catch (Exception e) {
                    Log.d("tag", "error" + e);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("tag", "Failed to read value.", databaseError.toException());
                reload.setVisibility(View.VISIBLE);
            }
        });
        final Intent intent=new Intent(getActivity(),PackDisplay.class);

        final Package aPackage = new Package();
        im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                FragmentManager manager = getFragmentManager();
//                manager.beginTransaction().replace(R.id.slider, aPackage).commit();
                startActivity(intent);
            }
        });
        im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);
            }
        });
        im3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);
            }
        });
        im4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);
            }
        });


        reload.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {

                Home home = new Home();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.slider, home).commit();


            }
        });
        return layout;
    }


    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

    }

    @Override
    public void onPause() {
        super.onPause();
        // stop auto scroll when onPause
        mPager.stopAutoScroll();
    }

    @Override
    public void onResume() {
        super.onResume();
        // start auto scroll when onResume
        mPager.startAutoScroll();
    }


}
