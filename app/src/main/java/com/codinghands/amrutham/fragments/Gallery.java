package com.codinghands.amrutham.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.activity.Tab;
import com.codinghands.amrutham.adapter.GalleryAdapter;
import com.codinghands.amrutham.domain.Gal;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Nadil on 5/30/2017.
 */

public class Gallery extends Fragment {
    private GalleryAdapter adapter;
    private RecyclerView recyclerView;
    LinearLayout reload;
   public static List<Gal> galList;
    private static final String TAG = "Ayur";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.gallerylist, container, false);
        reload = (LinearLayout) layout.findViewById(R.id.reload);
        galList = new ArrayList<>();
        recyclerView = (RecyclerView) layout.findViewById(R.id.gallist);

        ImageView load= (ImageView) layout.findViewById(R.id.loading);
        Glide.with(this).load(R.drawable.loading).asGif().into(load);



        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();

        DatabaseReference myRef3 = mDatabase.getReference().child("Gallery");
        myRef3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                galList.clear();
                reload.setVisibility(View.GONE);
                try {


                    ArrayList<Map<String, Object>> imgs = (ArrayList<Map<String, Object>>) dataSnapshot.child("images").getValue();
                    for (int i = 0; i < imgs.size(); i++) {


                        String imgg = String.valueOf(imgs.get(i));


                        Gal gal = new Gal(imgg);
                        galList.add(gal);

                    }

                } catch (Exception e) {
                    Log.d(TAG, "error" + e);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
                reload.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        });


        recyclerView.setNestedScrollingEnabled(false);
        adapter = new GalleryAdapter(getActivity(), galList);
        if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE){
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        }else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        }

        recyclerView.setAdapter(adapter);


        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Tab tab = new Tab();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.slider, tab).commit();


            }
        });
        return layout;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ? 5 : 3));
        super.onConfigurationChanged(newConfig);
    }
}