package com.codinghands.amrutham.fragments;


import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.activity.Mesg;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.util.HashMap;


/**
 * Created by Nadil on 5/31/2017.
 */

public class Msg extends android.support.v4.app.Fragment {
String feedbackurl;
    Mesg mesg;
    EditText name, email, phone, subject, message;
    TextView send;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.msg, container, false);

        name = (EditText) layout.findViewById(R.id.name);
        email = (EditText) layout.findViewById(R.id.email);
        phone = (EditText) layout.findViewById(R.id.mobile);
        subject = (EditText) layout.findViewById(R.id.subject);
        message = (EditText) layout.findViewById(R.id.message);
        send = (TextView) layout.findViewById(R.id.send);


        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference myRef7 = mDatabase.getReference().child("api");
        myRef7.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    feedbackurl = (String) dataSnapshot.child("feedbackurl").getValue();
                    Log.d("tag", "url" + feedbackurl);

                } catch (Exception e) {
                    Log.d("tag", "error" + e);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("tag", "Failed to read value.", databaseError.toException());
            }
        });
        getActivity().setTitle("Enquiry");


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String nm = name.getText().toString().trim();
                String em = email.getText().toString().trim();
                String ph = phone.getText().toString().trim();
                String sub = subject.getText().toString().trim();
                String mes = message.getText().toString().trim();

                if (nm.equals("") || em.equals("") || ph.equals("") || sub.equals("") || mes.equals("")) {

                    Toast.makeText(getActivity(), "Field should not be empty", Toast.LENGTH_SHORT).show();
                } else {
                    sendmessage(nm, em, ph, sub, mes);
                }
            }
        });

        return layout;
    }


    public void sendmessage(final String name1, final String email1, final String phone1, final String subject1, final String message1) {

        String url = feedbackurl;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name", name1);
        params.put("email", email1);
        params.put("phone", phone1);
        params.put("subject", subject1);
        params.put("message", message1);

        Log.d("login parameters", "json parameters" + params);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(params), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                Toast.makeText(getActivity(), "successful", Toast.LENGTH_SHORT).show();

                name.setText("");
                email.setText("");
                phone.setText("");
                subject.setText("");
                message.setText("");


                Log.d("response", "response" + response);

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("login volley", "failed");
                        Toast.makeText(getActivity(), "networkerror", Toast.LENGTH_SHORT).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);

    }


}
