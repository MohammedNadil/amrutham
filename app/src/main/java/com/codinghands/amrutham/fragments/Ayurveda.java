package com.codinghands.amrutham.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.adapter.CardAdapter;
import com.codinghands.amrutham.adapter.MyAdapter2;

import com.codinghands.amrutham.domain.Ayur;
import com.codinghands.amrutham.domain.Slidehome;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import me.relex.circleindicator.CircleIndicator;


/**
 * Created by Nadil on 5/30/2017.
 */

public class Ayurveda extends Fragment {
    private CardAdapter adapter;
    private RecyclerView recyclerView;
    private static final String TAG = "Ayur";

    TextView ades, head;
    LinearLayout reload;

    List<Ayur> ayurList;

    AutoScrollViewPager mPager2;

    MyAdapter2 myAdapter;


    List<Slidehome> slider2;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.ayurlist, container, false);
        ayurList=new ArrayList<>();
        slider2 = new ArrayList<>();
        ades = (TextView) layout.findViewById(R.id.ades);

        mPager2 = (AutoScrollViewPager) layout.findViewById(R.id.pager);
        recyclerView = (RecyclerView) layout.findViewById(R.id.rv_user_list);
        head = (TextView) layout.findViewById(R.id.head);
        reload = (LinearLayout) layout.findViewById(R.id.reload);
        ImageView load = (ImageView) layout.findViewById(R.id.loading);
        Glide.with(this).load(R.drawable.loading).asGif().into(load);


        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();


        final DatabaseReference myRef = mDatabase.getReference().child("Ayurveda");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ayurList.clear();
                reload.setVisibility(View.GONE);
                try {

                    Map<String, Object> ayurs = (Map<String, Object>) dataSnapshot.child("packages").getValue();

                    String a = (String) ayurs.get("description");
                    ades.setText(a);
                    ArrayList<Map<String, Object>> ayurs2 = (ArrayList<Map<String, Object>>) dataSnapshot.child("packages").child("packageArray").getValue();


                    for (int i = 0; i < ayurs2.size(); i++) {

                        String name = (String) ayurs2.get(i).get("name");
                        String img = (String) ayurs2.get(i).get("image");
                        String des = (String) ayurs2.get(i).get("descri");
                        Ayur ayur = new Ayur(des, img, name);
                        ayurList.add(ayur);

                        Log.d(TAG, "name" + ayurList);
                        Log.d(TAG, "ayur" + ayurs2);

                    }

                    recyclerView.setNestedScrollingEnabled(false);
                    adapter = new CardAdapter(getActivity(), ayurList);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setAdapter(adapter);


                } catch (Exception e) {

                    Log.d(TAG, "error" + e);

                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
                reload.setVisibility(View.VISIBLE);

            }
        });


        final DatabaseReference myRef6 = mDatabase.getReference().child("Ayurslider");
        myRef6.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                slider2.clear();

                try {

                    ArrayList<Map<String, Object>> ayurslider = (ArrayList<Map<String, Object>>) dataSnapshot.getValue();

                    Log.d("tag", "imagessss:" + ayurslider);

                    for (int i = 0; i < ayurslider.size(); i++) {


                        String imgs = String.valueOf(ayurslider.get(i));
                        Slidehome slidehome = new Slidehome(imgs);

                        slider2.add(slidehome);


                        myAdapter = new MyAdapter2(getActivity(), slider2);

                        mPager2.setAdapter(myAdapter);
                        mPager2.setOnPageChangeListener(new MyOnPageChangeListener());
                        myAdapter.notifyDataSetChanged();
                        mPager2.setInterval(2000);
                        mPager2.startAutoScroll();
                        CircleIndicator indicator = (CircleIndicator) layout.findViewById(R.id.indicator);
                        indicator.setViewPager(mPager2);
                        Log.d("tag", "ayurslider:" + slider2);
                    }

                } catch (Exception e) {
                    Log.d("tag", "error" + e);

                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("tag", "Failed to read value.", error.toException());

            }
        });


        return layout;
    }




    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

    }

    @Override
    public void onPause() {
        super.onPause();
        // stop auto scroll when onPause
        mPager2.stopAutoScroll();
    }

    @Override
    public void onResume() {
        super.onResume();
        // start auto scroll when onResume
        mPager2.startAutoScroll();
    }
}