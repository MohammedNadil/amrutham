package com.codinghands.amrutham.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.adapter.DesAdapter;
import com.codinghands.amrutham.domain.Des;
import com.codinghands.amrutham.domain.Pack;
import com.codinghands.amrutham.fragments.Package;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.codinghands.amrutham.adapter.PackAdapter.packs;


/**
 * Created by Nadil on 5/31/2017.
 */

public class Desview extends AppCompatActivity {
    private DesAdapter adapter;
    ImageView desimg;
    private RecyclerView recyclerView;
    private static final String TAG = "Ayur";

    int j;
    List<Des> des3;
    String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deslist);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        desimg = (ImageView) findViewById(R.id.desimg);

        if (getIntent() != null) {

            j = getIntent().getIntExtra("pos", 0);
            path = getIntent().getStringExtra("img");
        }
        des3 = new ArrayList<>();

        Pack pack = packs.get(j);

        getSupportActionBar().setTitle(pack.getNames());
        ArrayList<Map<String, Object>> des1 = pack.getDescription();

        Glide.with(this).load(path).placeholder(R.drawable.placeholder).into(desimg);

        for (int i = 0; i < des1.size(); i++) {

            Map<String, Object> des = (Map<String, Object>) des1.get(i);


            String title = (String) des.get("title");
            String content = (String) des.get("content");


            Des des2 = new Des(title, content);

            Log.d(TAG, "title" + title);

            des3.add(des2);
            Log.d(TAG, "daaata" + des3);
        }

        recyclerView = (RecyclerView) findViewById(R.id.deslist);
        recyclerView.setNestedScrollingEnabled(false);
        adapter = new DesAdapter(this, des3);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
