package com.codinghands.amrutham.activity;


import android.os.Bundle;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.adapter.MyAdapter3;

import static com.codinghands.amrutham.fragments.Gallery.galList;

/**
 * Created by Nadil on 5/31/2017.
 */

public class Fullview extends AppCompatActivity{

    int i;
    private static ViewPager mPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Gallery");


        i = getIntent().getIntExtra("pos", 0);

        init();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void init() {


        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new MyAdapter3(this, galList));
        mPager.setCurrentItem(i);
    }


}
