package com.codinghands.amrutham.activity;


import android.content.Intent;
import android.os.Bundle;

import android.os.Handler;
import android.support.design.internal.NavigationMenuView;
import android.support.v4.app.FragmentManager;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.widget.Toast;


import com.codinghands.amrutham.fragments.Ayurveda;
import com.codinghands.amrutham.fragments.Home;
import com.codinghands.amrutham.fragments.Package;
import com.codinghands.amrutham.R;
import com.google.android.gms.maps.MapView;
import com.google.firebase.database.FirebaseDatabase;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    static boolean calledAlready = false;
boolean doublebacktoexit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);



        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MapView mv = new MapView(getApplicationContext());
                    mv.onCreate(null);
                    mv.onPause();
                    mv.onDestroy();
                } catch (Exception ignored) {

                }
            }
        }).start();

        if (!calledAlready) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            calledAlready = true;
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));

        disableNavigationViewScrollbars(navigationView);


    }

    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (doublebacktoexit) {
            super.onBackPressed();
            return;

        }


        if(!drawer.isDrawerOpen(GravityCompat.START)){
        this.doublebacktoexit = true;
        Toast.makeText(this, "Please Click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doublebacktoexit = false;
            }
        }, 2000);

    }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home) {

            Home home = new Home();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.slider, home).commit();


        } else if (id == R.id.pack) {

//            Package aPackage = new Package();
//            FragmentManager manager = getSupportFragmentManager();
//            manager.beginTransaction().replace(R.id.slider, aPackage).commit();

            Intent intent=new Intent(this,PackDisplay.class);
            startActivity(intent);


        } else if (id == R.id.ayurveda) {

//
//            Ayurveda ayurveda = new Ayurveda();
//            FragmentManager manager = getSupportFragmentManager();
//            manager.beginTransaction().replace(R.id.slider, ayurveda).commit();

            Intent intent=new Intent(this,AyurDisplay.class);
            startActivity(intent);


        } else if (id == R.id.gallery) {


            Intent intent=new Intent(this,TabDisplay.class);
            startActivity(intent);

//            Tab tab = new Tab();
//            FragmentManager manager = getSupportFragmentManager();
//            manager.beginTransaction().replace(R.id.slider, tab).commit();


        } else if (id == R.id.cpolicy) {


            Intent intent=new Intent(this,Cancel.class);
            startActivity(intent);


        } else if (id == R.id.msg) {
//            Msg msg = new Msg();
//            FragmentManager manager = getSupportFragmentManager();
//            manager.beginTransaction().replace(R.id.slider, msg).commit();

            Intent intent=new Intent(this,Mesg.class);
            startActivity(intent);


        } else if (id == R.id.contactus) {
//            Contact contact = new Contact();
//            FragmentManager manager = getSupportFragmentManager();
//            manager.beginTransaction().replace(R.id.slider, contact).commit();

            Intent intent=new Intent(this,Contact.class);
            startActivity(intent);


        } else if (id == R.id.aboutus) {
//            About about = new About();
//            FragmentManager manager = getSupportFragmentManager();
//            manager.beginTransaction().replace(R.id.slider, about).commit();

            Intent intent=new Intent(this,About.class);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
