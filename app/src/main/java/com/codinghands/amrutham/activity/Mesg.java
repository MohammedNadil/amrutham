package com.codinghands.amrutham.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.codinghands.amrutham.R;
import com.codinghands.amrutham.domain.Pack;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.codinghands.amrutham.adapter.PackAdapter.packs;

/**
 * Created by Nadil on 6/21/2017.
 */

public class Mesg extends AppCompatActivity {

    public EditText name, email, phone, sub, mes;
    TextView send;
    String feedbackurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mesg);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Enquiry");


        name = (EditText) findViewById(R.id.name1);
        email = (EditText) findViewById(R.id.email1);
        phone = (EditText) findViewById(R.id.mobile1);
        sub = (EditText) findViewById(R.id.subject1);
        mes = (EditText) findViewById(R.id.message1);
        send = (TextView) findViewById(R.id.send1);


        if (getIntent() != null) {
            int i = getIntent().getIntExtra("pos", 0);
            boolean b = getIntent().getBooleanExtra("boolean", false);
            if (b) {
                Pack pack = packs.get(i);
                sub.setText(pack.getNames());
            } else {
                sub.setText("");
            }
        }

        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference myRef7 = mDatabase.getReference().child("api");
        myRef7.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    feedbackurl = (String) dataSnapshot.child("feedbackurl").getValue();
                    Log.d("tag", "url" + feedbackurl);

                } catch (Exception e) {
                    Log.d("tag", "error" + e);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("tag", "Failed to read value.", databaseError.toException());
            }
        });


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String nm = name.getText().toString().trim();
                String em = email.getText().toString().trim();
                String ph = phone.getText().toString().trim();
                String sub1 = sub.getText().toString().trim();
                String mes1 = mes.getText().toString().trim();

                if (nm.equals("") || em.equals("") || ph.equals("") || sub1.equals("") || mes1.equals("")) {

                    Toast.makeText(Mesg.this, "Field should not be empty", Toast.LENGTH_SHORT).show();
                } else {
                    sendmessage(nm, em, ph, sub1, mes1);
                }
            }
        });


    }


    private void sendmessage(final String name1, final String email1, final String phone1, final String subject1, final String message1) {

        String url = feedbackurl;

        Log.d("url", "furl" + url);


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name", name1);
        params.put("email", email1);
        params.put("phone", phone1);
        params.put("subject", subject1);
        params.put("message", message1);

        Log.d("login parameters", "json parameters" + params);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(params), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                try {

                    String message = String.valueOf(response.get("Message"));

                    if (message.equals("Success:Email send successfully")) {
                        Toast.makeText(Mesg.this, "Email send successfully", Toast.LENGTH_SHORT).show();
                        name.setText("");
                        email.setText("");
                        phone.setText("");
                        sub.setText("");
                        mes.setText("");


                    } else if (message.equals("Error:Invalid Email")) {
                        Toast.makeText(Mesg.this, "Invalid Email", Toast.LENGTH_SHORT).show();

                    } else if (message.equals("Error:Email cannot be send")) {
                        Toast.makeText(Mesg.this, "Email cannot be send", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Log.d("response", "response" + response);

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("login volley", "failed" + error);
                        Toast.makeText(Mesg.this, "networkerror", Toast.LENGTH_SHORT).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(Mesg.this);

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
