package com.codinghands.amrutham.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.codinghands.amrutham.R;

/**
 * Created by Nadil on 5/31/2017.
 */

public class About extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("About Us");

        WebView td = (WebView) findViewById(R.id.textd);

        String youtContentStr = String.valueOf(Html
                .fromHtml("<![CDATA[<body style=\"text-align:justify;color:gray;font-size:14\">"
                        +"<br>"+ getResources().getString(R.string.t1)+"<br><br>"+ getResources().getString(R.string.t2)+"<br><br>"+ getResources().getString(R.string.t3)
                        + "<br></body>]]>"));

        td.loadData(youtContentStr, "text/html", "utf-8");

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
