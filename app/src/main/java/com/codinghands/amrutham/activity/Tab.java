package com.codinghands.amrutham.activity;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codinghands.amrutham.R;
import com.codinghands.amrutham.fragments.Gallery;
import com.codinghands.amrutham.fragments.Vedio;


/**
 * Created by Nadil on 6/3/2017.
 */

public class Tab extends Fragment {

    private ViewPager mViewPager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.tab, container, false);
        getActivity().setTitle("Gallery");

        TabLayout tabLayout = (TabLayout) layout.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Photos"));
        tabLayout.addTab(tabLayout.newTab().setText("Videos"));
        tabLayout.setTabTextColors(Color.parseColor("#702f7d32"),Color.parseColor("#004e05"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mViewPager = (ViewPager) layout.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new MyPagerAdapter(getFragmentManager(), tabLayout.getTabCount()));
        tabLayout.setupWithViewPager(mViewPager);

        return layout;
    }


    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        int no;

        private String[] tabTitles = new String[]{"Photos", "Videos"};

        public MyPagerAdapter(FragmentManager fragmentManager, int no) {
            super(fragmentManager);
            this.no = no;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {

                case 0:
                    Gallery gallery = new Gallery();
                    return gallery;
                case 1:
                    Vedio vedio = new Vedio();
                    return vedio;
                default:
                    return null;

            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }


    }
}
