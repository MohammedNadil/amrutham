package com.codinghands.amrutham.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.codinghands.amrutham.R;
import com.codinghands.amrutham.fragments.Ayurveda;

/**
 * Created by Nadil on 6/30/2017.
 */

public class AyurDisplay extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.displayer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Ayurveda");

        Ayurveda ayurveda = new Ayurveda();
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.displayer, ayurveda).commit();



    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



}
